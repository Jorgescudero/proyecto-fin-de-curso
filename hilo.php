<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}

if (isset($_POST['back'])) {

    header("Location:foro.php");
}
$id = $_SESSION['idhilo'][0];
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Foros</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        h5{
            margin-left: 360px;
            margin-top: 20px;
        }
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light">
  		<a class="navbar-brand" href="#">Animal Home</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
 		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
    		<ul class="navbar-nav mr-auto">
				<?php
                            if ($_SESSION['rol']== "Cliente") {
                        ?>
                        <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                        <li class="nav-item active"><a class="nav-link"href="foro.php">Foro</a></li>
                        <li class="nav-item"><a class="nav-link" href="mascotas.php">Mascotas</a></li>
                        <li class="nav-item"><a class="nav-link" href="observacionescli.php">Observaciones</a></li>
                        <li class="nav-item"><a class="nav-link" href="perfil.php">Perfil</a></li>

                        <?php
                            }
                            if ($_SESSION['rol']=="Veterinario") {
                        ?>
                            <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                            <li class="nav-item active"><a class="nav-link" href="foro.php">Foro</a></li>
                            <li class="nav-item"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                        <?php
                            }
                        ?>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
            </form>
		</div>
	</nav>
    <div id="hilo">
        <div class="container-fluid">
            <form method="POST" action="">
                <button type="submit" style="margin-top: 10px;margin-left: 40px;font-size: 25px; width: 100px;" class="btn btn-success" name="back">&#8592;</button>
            </form>
            <div class="row" style="margin-top: 5px;">
                    <div class="col-md-6" style="margin-top: 10px;margin-left: 200px;margin-bottom: 10px;background-color: white;box-shadow: 0px 0px 8px grey; background-color: #ECF8E0;">
                        <h3 class="text-center"><?php echo $_SESSION['idhilo'][1]; ?></h3>
                        <p><?php echo $_SESSION['idhilo'][2]; ?></p>
                    </div>
            </div>
            <?php
                if ($_SESSION['rol']=='Cliente') {
                    $tipo = "Cliente";
                    $conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
                    if (mysqli_connect_errno()) {
                        printf("Conexión fallida %s\n", mysqli_connect_error());
                        exit();
                    }
                }
                if ($_SESSION['rol']=='Veterinario') {
                    $tipo = "Veterinario";
                    $conexion = mysqli_connect('localhost', 'Veterinario', 'Veterinario', 'veterinario');
                    if (mysqli_connect_errno()) {
                        printf("Conexión fallida %s\n", mysqli_connect_error());
                        exit();
                    }
                }
                $sql="SELECT idHilo,idMensaje,mensajetxt,fechaMensaje,usuario from mensajes where idHilo = '$id' order by fechaMensaje asc";
                $result = mysqli_query($conexion, $sql);
                    if(mysqli_num_rows($result) > 0) {
                        while ($registro = mysqli_fetch_row($result)) {
                ?>
                <div class="row" style="margin-top: 5px;">
                    <div class="col-md-6" style="margin-top: 10px;margin-left: 200px;margin-bottom: 10px;background-color: white;box-shadow: 0px 0px 8px grey; background-color: #ECF8E0;">
                        <p><?php echo $registro[4] .' : ' .$registro[2]; ?></p>
                        <p><small class="float-right"><b style="letter-spacing: 1px;">Fecha:  <?php echo $registro[3]; ?></b></small></p>
                    </div>
                </div>
                <?php
                        }
                    }else{
                        echo "<h5>Este hilo todavia no tiene mensajes</h5>";
                    }
                ?>
                <div class="col-md-6" style="margin-left: 170px; margin-top: 20px;">
                    <form action="controlmensajes.php" method="POST">
                        <div class="form-group">
                            <label for="comment">Comenta:</label> 
                            <textarea class="form-control border-dark" rows="5" name="comment" id="comment" cols="6"></textarea>
                        </div>
                       <button  type="submit" class="btn btn-dark" style="margin-top: 5px;" name="mensaje">Enviar</button>
                    </form>
                </div>

        </div>
    </div>
</body>
</html>