<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}

$conexion = mysqli_connect('localhost', 'Asistente', 'Asistente', 'veterinario');
            if (mysqli_connect_errno()) {
                printf("Conexión fallida %s\n", mysqli_connect_error());
                exit();
            }

?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Nueva Cita</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        #ncita{
            margin-top: 10px;
            margin-left: 200px;
            margin-bottom: 10px;
            background-color: white;
            box-shadow: 0px 0px 8px grey;
        }
        h3{
            margin-left: 200px;
            margin-top: 30px;
        }
        *{
            letter-spacing: 1px;
        }
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
    </head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Animal Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                <li class="nav-item active"><a class="nav-link" href="altacita.php">Organizar Citas</a></li>
                <li class="nav-item"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                <li class="nav-item"><a class="nav-link" href="listapacientes.php">Ver Pacientes</a></li>
            </ul>
                <form class="form-inline my-2 my-lg-0" action="" method="POST">
                    <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
                </form>
        </div>
    </nav>
    <div class="row">
                    <div class="col-9">
                        <form id="ncita" action="" method="POST" class="p-3">
                            <fieldset>
                            <legend>Nueva cita</legend>
                              <div class="form-group">
                                <label for="paciente" style="margin-top: 10px;">Paciente</label>
                                <select class="form-control border-right-0 border-top-0 border-left-0" id="paciente" name="paciente" placeholder="Nombre" required="required">
                                    <option value="vacio">Seleccione</option>
                                    <?php 
                                        $sql="SELECT usuNombre,usuApellidos,nomMascota,idMascota FROM usuarios,mascotas WHERE mascotas.dniPropietario = usuarios.dniUsu order by usuApellidos;";
                                        $result = mysqli_query ($conexion, $sql);
                                        if(mysqli_num_rows($result) > 0) {
                                            while ($registro = mysqli_fetch_row($result)) {

                                    ?>
                                    <option value=<?php echo $registro[3] ?>><?php echo $registro[1].",".$registro[0] ."-" .$registro[2]; ?></option>
                                    <?php

                                            }
                                        }

                                    ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="fecha">Fecha Cita</label>
                                <input type="date" class="form-control border-right-0 border-top-0 border-left-0" id="fecha" name="fecha" required="required" placeholder="dd/mm/yyyy" min="<?php echo date("Y-m-d"); ?>">
                              </div>
                              <div class="form-group">
                                <label for="hora">Hora</label>
                                <input type="time" class="form-control border-right-0 border-top-0 border-left-0" id="hora" name="hora" required="required" min="08:30" max="17:00">
                              </div>
                              <input type="submit" class="btn btn-primary" style="margin-bottom: 5px;" name="nuevacita" value="Registrar">
                            </fieldset>
                        </form>
                        <?php

                        if (isset($_POST['nuevacita'])) {
                            $idpaciente = $_POST['paciente'];
                            $fecha = $_POST['fecha'];
                            $hora = $_POST['hora'];
                            $sql1 = "INSERT INTO citas (idCita,citFecha,citHora,citMascota,citEstado,citObservacion) VALUES ('','$fecha','$hora','$idpaciente','Pendiente','Vacia');";
                            if (mysqli_query($conexion, $sql1)){
                                echo "<h3>Nueva cita organizada</h3>";
                            }else{
                                echo " <br> Error: " . $sql1 . "<br>" . mysqli_error($conexion);
                            }
                        }

                        ?>
                    </div>

    </div>
</body>
</html>