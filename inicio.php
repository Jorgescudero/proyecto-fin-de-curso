<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Inicio</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	img{
    		height: 550px;
    		width: 100%;
    		opacity: 0.7;
    	}
    	section{
    		border: solid  1px black;
    		background-color: black;
    	}
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light">
  		<a class="navbar-brand" href="#">Animal Home</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
 		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
    		<ul class="navbar-nav mr-auto">
						<?php
							if ($_SESSION['rol']== "Cliente") {
						?>
						<li class="nav-item active"><a class="nav-link" href="inicio.php">Inicio</a></li>
						<li class="nav-item"><a class="nav-link"href="foro.php">Foro</a></li>
						<li class="nav-item"><a class="nav-link" href="mascotas.php">Mascotas</a></li>
						<li class="nav-item"><a class="nav-link" href="observacionescli.php">Observaciones</a></li>
						<li class="nav-item"><a class="nav-link" href="perfil.php">Perfil</a></li>

						<?php
							}
							if ($_SESSION['rol']=="Veterinario") {
						?>
							<li class="nav-item active"><a class="nav-link" href="inicio.php">Inicio</a></li>
							<li class="nav-item"><a class="nav-link" href="foro.php">Foro</a></li>
							<li class="nav-item"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
						<?php
							}

							if ($_SESSION['rol']=="Asistente") {
						?>
							<li class="nav-item active"><a class="nav-link" href="inicio.php">Inicio</a></li>
							<li class="nav-item"><a class="nav-link" href="altacita.php">Organizar Citas</a></li>
							<li class="nav-item"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
							<li class="nav-item"><a class="nav-link" href="listapacientes.php">Ver Pacientes</a></li>
						<?php
							}
						?>
						
					</ul>
					<form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
                    </form>
		</div>
	</nav>
		
		<section id="demo" class="carousel slide d-none d-md-block" data-ride="carousel">
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="Imagenes/friends.jpg" alt="Max" class="mx-auto d-block">
                    <div class="carousel-caption">
                        <h2 style="color: white; letter-spacing: 2px;">Bienvenido a Animal Home</h2>
                        <h4 style="color: white; letter-spacing: 1px;">Esta web esta diseñada para el uso y disfrute de los dueños de gatos y perros</h4>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="Imagenes/kittens.jpg" alt="Luna" class="mx-auto d-block">
                    <div class="carousel-caption">
                        <h2 style="color: white; letter-spacing: 2px;">Desde esta web puede:</h2>
                        <h4 style="color: white; letter-spacing: 1px;">Consultar foros, observar las obsevaciones de su mascota e incluir a su mascota en su perfil</h4>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="Imagenes/rottweiler.jpg" alt="Ralph" class="mx-auto d-block">
                    <div class="carousel-caption">
                        <h2 style="color: white; letter-spacing: 2px;">Disfrute de Animal Home</h2>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </section>

</body>
</html>