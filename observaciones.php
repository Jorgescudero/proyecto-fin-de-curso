<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}
if (isset($_POST['back'])) {

    header("Location:vercitas.php");
}
$conexion = mysqli_connect('localhost', 'Veterinario', 'Veterinario', 'veterinario');
if (mysqli_connect_errno()) {
    printf("Conexión fallida %s\n", mysqli_connect_error());
    exit();
}
$idcita = $_SESSION['atenderpaciente'][0];
$hora = $_SESSION['atenderpaciente'][1];
$comentario="";
if (isset($_POST['atender'])) {
        $observaciones=$_POST['obs'];
        $sql="UPDATE citas SET citEstado='Atendida', citObservacion='$observaciones' WHERE idCita='$idcita' AND citHora='$hora';";
        if (mysqli_query($conexion, $sql)) {
            $comentario = "<p> Se han registrado las observaciones con éxito</p>";
            }
        else {
            $comentario = " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
        }
        header("Location:vercitas.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Citas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        td{
            text-align: center;
        }
        h1{
            text-align: center;
            margin-top: 50px;
        }
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
    </head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="#">Animal Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                            <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                            <li class="nav-item"><a class="nav-link" href="foro.php">Foro</a></li>
                            <li class="nav-item active"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
                    </form>
        </div>
    </nav>
    <div class="container-fluid">
        <form method="POST" action="">
            <button type="submit" style="margin-top: 10px;margin-left: 40px;font-size: 25px; width: 100px;" class="btn btn-success" name="back">&#8592;</button>
        </form>
        <div class="row">
        <form action="" method="POST" style="margin-left: 25%;">
        <fieldset>
            <legend>Atender cita</legend>
        <div class="table-responsive">
            <table class="table">
                <?php

                    $sql="SELECT usuNombre,usuApellidos,nomMascota,citHora,citFecha from usuarios,mascotas,citas where citas.idcita='$idcita' AND citas.citMascota=mascotas.idMascota AND mascotas.dniPropietario=usuarios.dniUsu;";
                    $result = mysqli_query ($conexion, $sql);
                    $registro=mysqli_fetch_row($result);


                    ?>
                <tr>
                    <th class="bg-dark text-light">Propietario</th>
                    <td class="table-light"><?php echo $registro[1] ."," .$registro[0]; ?></td>
                </tr>
                <tr>
                    <th class="bg-dark text-light">Nombre mascota</th>
                    <td class="table-light"><?php echo $registro[2]; ?></td>
                </tr>
                <tr>
                    <th class="bg-dark text-light">Fecha cita</th>
                    <td class="table-light"><?php echo $registro[4]; ?></td>
                </tr>
                <tr>
                    <th class="bg-dark text-light">Hora cita</th>
                    <td class="table-light"><?php echo $registro[3]; ?></td>
                </tr>
                <tr>
                    <th class="bg-dark text-light">Observaciones</th>
                    <td class="table-light"><textarea name="obs" placeholder="Escriba aquí las observaciones del paciente" style="box-sizing: border-box; width: 350px; height: 200px; resize: none; overflow: auto;" required="required"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="btn btn-primary" name="atender" value="Enviar"></td>
                </tr>
            </table>
        </div>
        </fieldset>
    </form>
    <?php
    echo $comentario;
     mysqli_close($conexion);
    ?>
            
        </div>
    </div>
</body>
</html>