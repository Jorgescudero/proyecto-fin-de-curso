<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Mascotas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	#remas{
    		margin-left: 200px;
    		margin-top: 30px;
    	}

    	#agregar{
    		display: none;
    	}

    	#mascotas{
    		display: block;
    	}

    	.form-control{
    		margin-left: 10px;
    		margin-right: 10px; 
    	}

    	.col-md-6{
    		margin-top: 5px;
    	}

    	label{
    		padding-left: 10px;
    	}
    	img{
    		margin-top: 5px;
    	}
    	nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
     <script type="text/javascript">
    	function visualizarmas(){
    		document.getElementById("mascotas").style.display='block';
    		document.getElementById("agregar").style.display='none';
    		document.getElementById("nombre").value='';
    		document.getElementById("fecha").value='';
    		document.getElementById("Sexo").value='Macho';
    		document.getElementById("Castrado").value='Si';
    		document.getElementById("Tipo").value='Perro';
    	}

    	function visualizarregistro(){
    		document.getElementById("mascotas").style.display='none';
    		document.getElementById("agregar").style.display='block';
    	}
    </script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light">
  		<a class="navbar-brand" href="#">Animal Home</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
 		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
    		<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
				<li class="nav-item"><a class="nav-link"href="foro.php">Foro</a></li>
				<li class="nav-item active"><a class="nav-link" href="mascotas.php">Mascotas</a></li>
				<li class="nav-item"><a class="nav-link" href="observacionescli.php">Observaciones</a></li>
				<li class="nav-item"><a class="nav-link" href="perfil.php">Perfil</a></li>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
            </form>
		</div>
	</nav>
	<div id="agregar">
		<div class="row">
					<div class="col-9">
						<form id="remas" action="controlmascotas.php" method="POST">
							<fieldset>
							<legend>Registro de mascotas</legend>
							  <div class="form-group">
							    <label for="nombre" style="margin-top: 10px;">Nombre</label>
							    <input type="text" class="form-control" id="nombre" name="nombremascota" placeholder="Nombre" required="required">
							  </div>
							  <div class="form-group">
							    <label for="fecha">Fecha Nacimiento</label>
							    <input type="date" class="form-control" id="fecha" name="fecha" required="required" placeholder="dd/mm/yyyy" max="<?php echo date("Y-m-d"); ?>">
							  </div>
							  <div class="form-group">
							    <label for="Sexo">Sexo</label>
							    <select class="form-control" id="Sexo" name="Sexo" required="required">
							    	<option value="Macho">Macho</option>
							    	<option value="Hembra">Hembra</option>
							    </select>
							  </div>
							  <div class="form-group">
							    <label for="Castrado">Castrado</label>
							    <select class="form-control" id="Castrado" name="Castrado" required="required">
							    	<option value="Si">Si</option>
							    	<option value="No">No</option>
							    </select>
							  </div>
							  <div class="form-group">
							    <label for="Tipo">Tipo</label>
							    <select class="form-control" id="Tipo" name="Tipo" required="required">
							    	<option value="Perro">Perro</option>
							    	<option value="Gato">Gato</option>
							    </select>
							  </div>
							  <input type="submit" class="btn btn-primary" style="margin-left: 10px; margin-bottom: 5px;" name="Registrar" value="Registrar">
							  <button type="button" class="btn btn-danger" style="margin-left: 10px; margin-bottom: 5px;" name="cancelar" onclick="visualizarmas()">Cancelar</button>
							</fieldset>
						</form>
					</div>
				</div>
	</div>
	<div id="mascotas">
		<div class="container-fluid">
		<?php
			$conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
			if (mysqli_connect_errno()) {
			    printf("Conexión fallida %s\n", mysqli_connect_error());
			    exit();
			}
			$dni=$_SESSION['nif'];
			$sql = "SELECT usuNombre,idMascota,nomMascota,fechaNacMascota,sexoMas,castrado,tipoMas,usuApellidos FROM usuarios,mascotas WHERE dniPropietario = '$dni' AND dniUsu = '$dni'";
			$result = mysqli_query ($conexion, $sql);
			if(mysqli_num_rows($result) > 0) {
				while ($registro = mysqli_fetch_row($result)) {
					$date= date('d-m-Y', strtotime($registro[3]));
					?>
					
					<div class="row">
					<?php
					if ($registro[6]=="Perro") {
						?>
					<div class="col-md-2">
            			<img src="Imagenes/Perro.jpg" class="d-none d-md-block text-md-left img-thumbnail" style="border-color: black;margin-bottom: 20px; margin-left: 100px; margin-top: 10px;" >
            		</div>
						<?php
					}else if ($registro[6]=="Gato") {
						?>
					<div class="col-md-2">
            			<img src="Imagenes/Gato.png" class="d-none d-md-block text-md-left img-thumbnail" style="border-color: black;margin-bottom: 20px; margin-left: 100px; margin-top: 10px;" >
            		</div>
						<?php
					}
					?>
					<div class="col-md-6" style="margin-top: 10px;margin-left: 100px;margin-bottom: 10px;background-color: white;box-shadow: 0px 0px 8px grey; background-color: #ECF8E0;">
		            	<p style="margin-top: 5px;">Nombre: <?php echo $registro[2]; ?>.</p>
		            	<p>Sexo: <?php echo $registro[4]; ?>.</p>
		            	<p>Fecha Nacimiento: <?php echo $date; ?></p>
		            	<p>Castrado: <?php echo $registro[5]; ?></p>
		            	<p>Dueño: <?php echo $registro[0]." " .$registro[7]; ?></p>
		            	<form action="controlmascotas.php" method="POST"><button style="margin-bottom: 5px;" class="btn btn-danger" type="submit" name="eliminar" value="<?php echo($registro[1]) ?>">Eliminar</button></form>
		            	
		            </div>



					<?php
					echo "</div>";
				}
			}else{
				echo "<h3 style='margin-top:20px; margin-left:30px;'>No has ingresado ninguna mascota</h3>";
			}
			?>
			<button type="button" style="margin-top: 10px;margin-left: 100px;" class="btn btn-success" onclick="visualizarregistro()">Insertar Mascota</button>
			
	</div>
	</div>
	<?php
	
	mysqli_close($conexion);

	?>

</body>
</html>