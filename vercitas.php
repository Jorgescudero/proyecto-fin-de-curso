<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}
if (isset($_POST['observacion'])) {
            $array = $_POST['observacion'];
            foreach ($array as $value) {
                $pac=explode(",", $value);
            }
            $_SESSION['atenderpaciente'] = $pac;
           var_dump($pac);
           header("Location:observaciones.php");
        }
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Citas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        td{
            text-align: center;
        }
        h1{
            text-align: center;
            margin-top: 50px;
        }
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
            background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
    </head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="#">Animal Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                        <?php
                            if ($_SESSION['rol']=="Veterinario") {
                                $conexion = mysqli_connect('localhost', 'Veterinario', 'Veterinario', 'veterinario');
                                if (mysqli_connect_errno()) {
                                    printf("Conexión fallida %s\n", mysqli_connect_error());
                                    exit();
                                }
                        ?>
                            <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                            <li class="nav-item"><a class="nav-link" href="foro.php">Foro</a></li>
                            <li class="nav-item active"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                        <?php
                            }

                            if ($_SESSION['rol']=="Asistente") {

                                $conexion = mysqli_connect('localhost', 'Asistente', 'Asistente', 'veterinario');
                                if (mysqli_connect_errno()) {
                                    printf("Conexión fallida %s\n", mysqli_connect_error());
                                    exit();
                                }
                        ?>
                            <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                            <li class="nav-item"><a class="nav-link" href="altacita.php">Organizar Citas</a></li>
                            <li class="nav-item active"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                            <li class="nav-item"><a class="nav-link" href="listapacientes.php">Ver Pacientes</a></li>
                        <?php
                            }
                        ?>
                        
                    </ul>
                    <form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
                    </form>
        </div>
    </nav>
    <div class="form-group row float-right" style="margin-right: 50px; margin-top: 20px;">
        <form action="" method="POST">
            <div class="col-xs-2">
                <label for="fechabus">Nueva fecha</label>
                <input class="form-control" id="fechabus" type="date" name="dia">
                <button class="btn btn-success btn-sm" type="submit" name="buscar" style="margin-top: 5px;">Buscar</button>
            </div>
        </form>
    </div>
        <div class="container-fluid">
            <?php
                if (isset($_POST['dia'])==false) {
                    $dia = date("d-m-Y");
                    $diabusqueda = date("Y-m-d");
                    echo "<h1>Citas dia " .$dia ."</h1>";
                    if ($_SESSION['rol']=="Asistente") {
                        $sql="SELECT usuNombre,usuApellidos,nomMascota,citHora,citEstado,citObservacion from usuarios,mascotas,citas where citas.citMascota = mascotas.idMascota AND mascotas.dniPropietario=usuarios.dniUsu AND citas.citFecha = '$diabusqueda' order by citas.citHora;";
                        ?>
                        <div class="table-responsive">
                                    <table class="table table-bordered bg-light">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Apellidos</th>
                                                <th>Mascota</th>
                                                <th>Hora</th>
                                                <th>Estado</th>
                                                <th>Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php
                        $result = mysqli_query ($conexion, $sql);
                        if(mysqli_num_rows($result) > 0) {
                            while ($registro = mysqli_fetch_row($result)) {
                                ?>
                                <tr>
                                    <td><?php echo $registro[0] ?></td>
                                    <td><?php echo $registro[1] ?></td>
                                    <td><?php echo $registro[2] ?></td>
                                    <td><?php echo $registro[3] ?></td>
                                    <td><?php echo $registro[4] ?></td>
                                    <td><?php echo $registro[5] ?></td>
                                </tr>
                                <?php
                            }
                        }else{
                            echo "<tr><td colspan='6'>No hay citas</td></tr>";
                        }
                        echo "</tbody></table>
                                </div>";
                    }
                    if ($_SESSION['rol']=="Veterinario") {
                        $sql="SELECT usuNombre,usuApellidos,nomMascota,citHora,citEstado,citObservacion,idCita,citFecha from usuarios,mascotas,citas where citas.citMascota = mascotas.idMascota AND mascotas.dniPropietario=usuarios.dniUsu AND citas.citFecha = '$diabusqueda' order by citas.citHora;";
                        ?>
                        <div class="table-responsive ">
                                    <table class="table table-bordered bg-light">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Apellidos</th>
                                                <th>Mascota</th>
                                                <th>Hora</th>
                                                <th>Estado</th>
                                                <th>Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php
                        $result = mysqli_query ($conexion, $sql);
                        if(mysqli_num_rows($result) > 0) {
                            while ($registro = mysqli_fetch_row($result)) {
                                ?>
                                <tr>
                                    <td><?php echo $registro[0] ?></td>
                                    <td><?php echo $registro[1] ?></td>
                                    <td><?php echo $registro[2] ?></td>
                                    <td><?php echo $registro[3] ?></td>
                                    <td><?php echo $registro[4] ?></td>
                                    <td><?php echo $registro[5] ?></td>
                        <?php
                                if ($registro[4]=="Pendiente" && $registro[7]==$diabusqueda) {
                        ?>
                                <td><form method="POST" action=""><button class="btn btn-info" type="submit" name="observacion[]" value=<?php echo $registro[6].",".$registro[3];?> >Atender</button></form></td>

                                
                                <?php
                            }else{
                                echo "<td></td>";
                            }
                            echo "</tr>";
                            }
                        }else{
                            echo "<tr><td colspan='7'>No hay citas</td></tr>";
                        }
                        echo "</tbody></table>
                                </div>";
                    }
                }else{
                    $diabusqueda = $_POST['dia'];
                    $dia= date('d-m-Y', strtotime($diabusqueda));
                    echo "<h1>Citas dia " .$dia ."</h1>";
                    if ($_SESSION['rol']=="Asistente") {
                        $sql="SELECT usuNombre,usuApellidos,nomMascota,citHora,citEstado,citObservacion from usuarios,mascotas,citas where citas.citMascota = mascotas.idMascota AND mascotas.dniPropietario=usuarios.dniUsu AND citas.citFecha = '$diabusqueda' order by citas.citHora;";
                        ?>
                        <div class="table-responsive">
                                    <table class="table table-bordered bg-light">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Apellidos</th>
                                                <th>Mascota</th>
                                                <th>Hora</th>
                                                <th>Estado</th>
                                                <th>Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php
                        $result = mysqli_query ($conexion, $sql);
                        if(mysqli_num_rows($result) > 0) {
                            while ($registro = mysqli_fetch_row($result)) {
                                ?>
                                <tr>
                                    <td><?php echo $registro[0] ?></td>
                                    <td><?php echo $registro[1] ?></td>
                                    <td><?php echo $registro[2] ?></td>
                                    <td><?php echo $registro[3] ?></td>
                                    <td><?php echo $registro[4] ?></td>
                                    <td><?php echo $registro[5] ?></td>
                                </tr>
                                <?php
                            }
                        }else{
                            echo "<tr><td colspan='6'>No hay citas</td></tr>";
                        }
                        echo "</tbody></table>
                                </div>";
                    }
                    if ($_SESSION['rol']=="Veterinario") {
                        $sql="SELECT usuNombre,usuApellidos,nomMascota,citHora,citEstado,citObservacion,idCita,citFecha from usuarios,mascotas,citas where citas.citMascota = mascotas.idMascota AND mascotas.dniPropietario=usuarios.dniUsu AND citas.citFecha = '$diabusqueda' order by citas.citHora;";
                        ?>
                        <div class="table-responsive ">
                                    <table class="table table-bordered bg-light">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Apellidos</th>
                                                <th>Mascota</th>
                                                <th>Hora</th>
                                                <th>Estado</th>
                                                <th>Observacion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php
                        $result = mysqli_query ($conexion, $sql);
                        if(mysqli_num_rows($result) > 0) {
                            while ($registro = mysqli_fetch_row($result)) {
                                ?>
                                <tr>
                                    <td><?php echo $registro[0] ?></td>
                                    <td><?php echo $registro[1] ?></td>
                                    <td><?php echo $registro[2] ?></td>
                                    <td><?php echo $registro[3] ?></td>
                                    <td><?php echo $registro[4] ?></td>
                                    <td><?php echo $registro[5] ?></td>
                        <?php
                                if ($registro[4]=="Pendiente" && $registro[7]==$diabusqueda) {
                        ?>
                                <td><form method="POST" action=""><button class="btn btn-info" type="submit" name="observacion[]" value=<?php echo $registro[6].",".$registro[3];?> >Atender</button></form></td>

                                
                                <?php
                            }else{
                                echo "<td></td>";
                            }
                            echo "</tr>";
                            }
                        }else{
                            echo "<tr><td colspan='7'>No hay citas</td></tr>";
                        }
                        echo "</tbody></table>
                                </div>";
                    }

                }
            ?>
        </div>
</body>
</html>