<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}

$conexion = mysqli_connect('localhost', 'Asistente', 'Asistente', 'veterinario');
            if (mysqli_connect_errno()) {
                printf("Conexión fallida %s\n", mysqli_connect_error());
                exit();
            }

?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Nueva Cita</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
    </head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="#">Animal Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                <li class="nav-item"><a class="nav-link" href="altacita.php">Organizar Citas</a></li>
                <li class="nav-item"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                <li class="nav-item active"><a class="nav-link" href="listapacientes.php">Ver Pacientes</a></li>
            </ul>
                <form class="form-inline my-2 my-lg-0" action="" method="POST">
                    <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
                </form>
        </div>
    </nav>
    <div class="container-fluid">
        <h1 style="text-align: center; margin-top: 10px;margin-bottom: 20px;">Tus pacientes</h1>
         <div class="table-responsive">
            <table class="table table-bordered bg-light">
                <thead class="thead-dark">
                    <tr>
                        <th>Apellidos</th>
                        <th>Nombre</th>
                        <th>Mascota</th>
                        <th>Fecha Nacimiento</th>
                        <th>Sexo</th>
                        <th>Castrado</th>
                        <th>Tipo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql="SELECT usuNombre,usuApellidos,nomMascota,fechaNacMascota,sexoMas,castrado,tipoMas from usuarios,mascotas where mascotas.dniPropietario = usuarios.dniUsu order by usuApellidos;";
                    $result = mysqli_query ($conexion, $sql);
                        if(mysqli_num_rows($result) > 0) {
                            while ($registro = mysqli_fetch_row($result)) {
                    ?>
                                <tr>
                                    <td><?php echo $registro[1] ?></td>
                                    <td><?php echo $registro[0] ?></td>
                                    <td><?php echo $registro[2] ?></td>
                                    <td><?php echo $registro[3] ?></td>
                                    <td><?php echo $registro[4] ?></td>
                                    <td><?php echo $registro[5] ?></td>
                                    <td><?php echo $registro[6] ?></td>
                                </tr>
                                <?php
                            }
                        }else{
                            echo "<tr><td colspan='7'>No has tenido ninguna cita</td></tr>";
                        }
                    ?>
    </div>
</body>
</html>