<?php
session_start();
$errorinicio = "";
$conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}
if (isset($_POST["inisesion"])) {
	$usuario = $_POST["nombreusu"];
	$password = $_POST["passwordusu"];
	$passwordencryp= hash_hmac('sha512', $password, 'Animalhome');
	$sql = "SELECT dniUsu,usuTipo FROM usuarios WHERE usuLogin = '$usuario' AND usuPassword = '$passwordencryp'";
	$result = mysqli_query ($conexion, $sql);
	if(mysqli_num_rows($result) > 0) {
		while ($registro = mysqli_fetch_row($result)) {
			$nif=$registro[0];
					          $rol=$registro[1];
					   }
								
						$_SESSION['nif']="$nif";
						  $_SESSION['rol']="$rol";
						  $_SESSION['user']=$usuario;
						  header("Location:inicio.php");
						        
					     
					   exit();
	}
	else {
		$errorinicio ="<h6>El usuario o contraseña no son correctos, vuelva a introducirlos</h6>";
	}

}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	#botones{
    		padding-top: 10px;
    	}

    	#iniciodesesion{
    			display: block;
    	}

    	#registro{
    		display: none;
    	}

    	#is{
    		margin-top: 10px;
    		margin-left: 33%;
    		background-color: white;
    		box-shadow: 0px 0px 8px grey;
    	}

    	#re{
    		margin-top: 10px;
    		margin-left: 33%;
    		margin-bottom: 10px;
    		background-color: white;
    		box-shadow: 0px 0px 8px grey;
    	}

    	.jumbotron{
    		background-image: linear-gradient(to right,lightblue, lightgreen);
    		margin-bottom: 0px;
    		border-bottom: 1px solid;
    		border-color: lightgreen;
    	}

    	label{
    		margin-left: 10px;
    	}

    	body{
    		 background-image: linear-gradient(to left,lightblue, lightgreen);
    	}
    	h6{
    		text-align: center;
    		margin-top: 5px;
    	}
    	*{
            letter-spacing: 1px;
        }
    </style>
    <script type="text/javascript">
    	function ri(){
    		document.getElementById("iniciodesesion").style.display='block';
    		document.getElementById("registro").style.display='none';
    		document.getElementById("nombrenuevo").value='';
    		document.getElementById("apellidos").value='';
    		document.getElementById("dni").value='';
    		document.getElementById("Telefono").value='';
    		document.getElementById("Email").value='';
    		document.getElementById("nombreusun").value='';
    		document.getElementById("password").value='';
    		document.getElementById("reppassword").value='';
    		document.getElementById('reppassword').style.border="";
			document.getElementById('avisopass').innerHTML="";
			document.getElementById('dni').style.border="";
		  	document.getElementById('avisodni').innerHTML="";
		  	document.getElementById('Telefono').style.border="";
		  	document.getElementById('avisotlf').innerHTML="";
		  	document.getElementById('Email').style.border="";
		    document.getElementById('avisomail').innerHTML="";
    	}

    	function rr(){
    		document.getElementById("iniciodesesion").style.display='none';
    		document.getElementById("registro").style.display='block';
    		document.getElementById("nombreusu").value='';
    		document.getElementById("passwordusu").value='';
    	}
    </script>
</head>
<body>
	<header class="jumbotron text-center">
            <h1>Animal Home</h1>
            <h4>Tu red de contacto con dueños y veterinarios</h4>
    </header>
    <section>
    	<div class="container">
		<form id="botones" class="row justify-content-center">
			<button type="button" class="btn btn-primary" onclick="ri()">Inicio Sesion</button><button type="button" class="btn btn-primary" style="margin-left: 3px;" onclick="rr()">Registrarse</button>
		</form>
		<div id="iniciodesesion">
				<div class="row">
					<div class="col-9">
						<form id="is" action="" method="POST" class="p-3">
								<div class="form-group">
						    		<label for="nombreusu">Nombre de Usuario</label>
						    		<input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="nombreusu" name="nombreusu" placeholder="Nombre de Usuario">
						  		</div>
						  		<div class="form-group">
						    		<label for="password">Contraseña</label>
						    		<input type="password" class="form-control border-right-0 border-top-0 border-left-0" id="passwordusu" name="passwordusu" placeholder="Password">
						  		</div>
						  		<p><input type="submit" class="btn btn-primary float-right" style="margin-right: 10px; margin-bottom: 20px;" name="inisesion" value="Inicio de Sesion"></input><p>
						  		<p><a href="recuperarcontra.php" style="margin-left: 7px;">¿Has olvidado tu contraseña?</a></p>
						</form>
						<?php echo $errorinicio;?>
					</div>
				</div>
		</div>
		<div id="registro">
			<div class="row">
					<div class="col-9">
						<form id="re" action="" onsubmit="validar()" method="POST" class="p-3">
						  <div class="form-group">
						    <label for="nombrenuevo">Nombre</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="nombrenuevo" name="nombre" placeholder="Nombre" required="required">
						  </div>
						  <div class="form-group">
						    <label for="apellidos">Apellidos</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="apellidos" name="apellidos" placeholder="apellidos" required="required">
						  </div>
						  <div class="form-group">
						    <label for="dni">DNI</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="dni" name="dni" placeholder="DNI" onblur="valdni()" required="required"><span id="avisodni" ></span>
						  </div>
						  <div class="form-group">
						    <label for="Telefono">Telefono</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="Telefono" name="Telefono" placeholder="Telefono" onblur="valtlf()" required="required"><span id="avisotlf"></span>
						  </div>
						  <div class="form-group">
						    <label for="Email">Email</label>
						    <input type="email" class="form-control border-right-0 border-top-0 border-left-0" id="Email" name="Email" placeholder="Email" onblur="valmail()" required="required"><span id="avisomail"></span>
						  </div>
						  <div class="form-group">
						    <label for="nombreusun">Nombre de usuario</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="nombreusun" name="nombreusun" placeholder="Nombre de Usuario" required="required">
						  </div>
						  <div class="form-group">
						    <label for="password">Contraseña</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="password" name="password" placeholder="Contraseña" required="required">
						  </div>
						  <div class="form-group">
						    <label for="reppassword">Repite Contraseña</label>
						    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="reppassword" name="reppassword" placeholder="Repite Contraseña" required="required" onblur="valpass()"><span id="avisopass"></span>
						  </div>
						  <input type="submit" class="btn btn-primary" style="margin-left: 10px; margin-bottom: 5px;" name="registrarse" value="Registrarse">
						</form>
					</div>
				</div>
		</div>
	</div>
	<?php
	if (isset($_POST["registrarse"])) {
		$dni = $_POST["dni"];
		$usulog = $_POST["nombreusun"];
		$usupassn = $_POST["password"];
		$usuNombre = $_POST["nombre"];
		$usuApellidos = $_POST["apellidos"];
		$usuTlf = $_POST["Telefono"];
		$usuCorreo = $_POST["Email"];
		$passwordencryp= hash_hmac('sha512', $usupassn, 'Animalhome');

		$sql="INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usuTipo,usuNombre,usuApellidos,usuTelef,usuCorreo) VALUES ('$dni','$usulog','$passwordencryp','Activo','Cliente','$usuNombre','$usuApellidos','$usuTlf','$usuCorreo');";
		if (mysqli_query($conexion, $sql)){
			echo "<h6>Gracias por registrarse en Animal Home</h6>";
			$asunto = "Creacion cuenta en Animal Home";
			$texto = "Bienvenido a Animal Home,\r\nEsperemos que su estancia aqui sea lo mejor posible.";
			$headers = "MIME-Version: 1.0" ."\r\n";
			$headers .= "Content-type: text/html; charset=utf-8" ."\r\n";
			$headers .= "From:noreplyanimalhome@gmail.com";
			mail($usuCorreo,$asunto,$texto,$headers);
		}else{
			echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
		}
	}
	mysqli_close($conexion);
?>
</section>

<script>
		
		function validar() {
			if (valdni() && valtlf() && valmail() && valpass()) {
				return true;
			}
			else {
				alert ("Datos erróneos, indtroducir de nuevo");
				return false;
			}
		}

		function valdni() {
			var nif = document.getElementById("dni").value;
			var expresion_regular_dni
		 
		  	expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
		 
		  	if (expresion_regular_dni.test (nif) == true) {
		    	document.getElementById('avisodni').innerHTML=" &check; DNI correcto";
		    	return true;
		  	}
		  	else {
		  		document.getElementById('avisodni').innerHTML=" &cross; DNI erróneo, formato no válido";
		  		return false;
		   	}	
		}

		function valtlf() {
			var regexptlf = /^[56789]\d{8}/
			var tlf = document.getElementById("Telefono").value;
			if (tlf.match(regexptlf)) {
		    	document.getElementById('avisotlf').innerHTML=" &check; Teléfono correcto";
				return true;
			}
			else {
		  		document.getElementById('avisotlf').innerHTML=" &cross; Teléfono no válido";
				return false;
			}
		}

		function valmail() {
			var regexpmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
			var mail = document.getElementById("Email").value
			if (mail.match(regexpmail)) {
		    	document.getElementById('avisomail').innerHTML=" &check; Email correcto";
				return true;
			}
			else {
		    	document.getElementById('avisomail').innerHTML=" &cross; Email incorrecto";
				return false;
			}
		}

		function valpass() {
			var contra=document.getElementById("password").value;
			var rcontra=document.getElementById("reppassword").value;

			if (contra===rcontra) {
				document.getElementById('avisopass').innerHTML=" &check; Contraseña correcta";
				return true;
			}
			else {
				document.getElementById('avisopass').innerHTML=" &cross; Contraseña incorrecta";
				return false;
			}
		}
</script>	
</body>
</html>
