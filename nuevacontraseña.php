<?php
session_start();

$conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	body{
    		background-image: linear-gradient(to left,lightblue, lightgreen);
    		size: cover;
    	}
    	#codigobus{
			margin-top: 10px;
    		margin-left: 33%;
    		margin-bottom: 10px;
    		background-color: white;
    		box-shadow: 0px 0px 8px grey;
    	}
    	#nuevacontraseña{
			margin-top: 10px;
    		margin-top: 10px;
    		margin-left: 33%;
    		margin-bottom: 10px;
    		background-color: white;
    		box-shadow: 0px 0px 8px grey;
    	}
    	#cajacodigo{
    		display: block;
    	}

    	#password{
    		display: none;
    	}
    	.jumbotron{
    		background-image: linear-gradient(to right,lightblue, lightgreen);
    	}
    	*{
            letter-spacing: 1px;
        }
    </style>
    <script type="text/javascript">
    	function comprobarcodigo(){
    		var codigo = <?php echo $_SESSION['codigo'];?>;
    		var comprobar = document.getElementById("codigon").value;
    		if (codigo==comprobar) {
    			document.getElementById("cajacodigo").style.display='none';
    			document.getElementById("password").style.display='block';
    		}else{
    			document.getElementById('respuestaerror').innerHTML=" &check; Codigo incorrecto";
    		}
    	}

    	function valpass() {
			var contra=document.getElementById("npassword").value;
			var rcontra=document.getElementById("reppassword").value;

			if (contra===rcontra) {
				return true;
			}
			else {
				document.getElementById('avisopass').innerHTML=" &cross; Contraseña incorrecta";
				return false;
			}
		}
    </script>
</head>
<body>
	<header class="jumbotron text-center">
            <h1>Animal Home</h1>
            <h4>Tu red de contacto con dueños y veterinarios</h4>
    </header>
    <div class="container">
		<div id="cajacodigo">
			<div class="row">
						<div class="col-9">
							<form action="" method="POST" id="codigobus" class="p-3">
								<div class="form-group">
									<label style="margin-left:5px" for="emailbus">Introuce el codigo que se te ha enviado al correo</label>
									<input type="number" class="form-control border-right-0 border-top-0 border-left-0" id="codigon" name="codigon" required="required">
								</div>
								<span id="respuestaerror"></span>
								<button type="button" class="btn btn-primary" style="margin-left: 10px; margin-bottom: 5px;" name="inicodigo" onclick="comprobarcodigo()">Comprobar</button>
								
							</form>
						</div>
			</div>
		</div>
		<div id="password">
			<div class="row">
						<div class="col-9">
							<form action="" method="POST" id="nuevacontraseña" onsubmit="valpass()" class="p-3">
								<div class="form-group">
								    <label for="npassword">Contraseña</label>
								    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="npassword" name="npassword" placeholder="Contraseña" required="required">
								</div>
								<div class="form-group">
								    <label for="reppassword">Repite Contraseña</label>
								    <input type="text" class="form-control border-right-0 border-top-0 border-left-0" id="reppassword" name="reppassword" placeholder="Repite Contraseña" required="required"><span id="avisopass"></span>
						  		</div>
						  		<input type="submit" class="btn btn-primary" style="margin-left: 10px; margin-bottom: 5px;" name="insertar" value="Insertar">
							</form>
						</div>
						<?php
							if (isset($_POST['insertar'])) {
								$mail = $_SESSION['email'];
								$nuevacon = $_POST['npassword'];
								$nencrypt = hash_hmac('sha512', $nuevacon, 'Animalhome');
								$sql="UPDATE usuarios SET usuPassword='$nencrypt' WHERE usuCorreo='$mail';";
								if (mysqli_query($conexion, $sql)) {
					                header("Location:index.php");
					            }
						        else {
						            echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
						        }
							}
						?>
			</div>
		</div>
	</div>
</body>
</html>
