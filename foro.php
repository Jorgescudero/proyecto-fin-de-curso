<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}
if (isset($_POST['id'])) {
    foreach ($_POST['id'] as $value) {
            $pac=explode(",", $value);
        }
        $_SESSION['idhilo']=$pac;
        var_dump($_SESSION['idhilo']);
        header("Location:hilo.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Foros</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        #rehilo{
            margin-left: 200px;
            margin-top: 30px;
        }
        #empezar{
            display: none;
        }

        #listahilos{
            display: block;
        }
        nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
    <script type="text/javascript">
        function visualizarrehilo(){
            document.getElementById("empezar").style.display='block';
            document.getElementById("listahilos").style.display='none';
            document.getElementById("titulo").value='';
            document.getElementById("mensaje").value='';
        }
        function visualizarhilos(){
            document.getElementById("empezar").style.display='none';
            document.getElementById("listahilos").style.display='block';
        }
    </script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light">
  		<a class="navbar-brand" href="#">Animal Home</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
 		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
    		<ul class="navbar-nav mr-auto">
				<?php
                            if ($_SESSION['rol']== "Cliente") {
                        ?>
                        <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                        <li class="nav-item active"><a class="nav-link"href="foro.php">Foro</a></li>
                        <li class="nav-item"><a class="nav-link" href="mascotas.php">Mascotas</a></li>
                        <li class="nav-item"><a class="nav-link" href="observacionescli.php">Observaciones</a></li>
                        <li class="nav-item"><a class="nav-link" href="perfil.php">Perfil</a></li>

                        <?php
                            }
                            if ($_SESSION['rol']=="Veterinario") {
                        ?>
                            <li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
                            <li class="nav-item active"><a class="nav-link" href="foro.php">Foro</a></li>
                            <li class="nav-item"><a class="nav-link" href="vercitas.php">Ver Citas</a></li>
                        <?php
                            }
                        ?>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
            </form>
		</div>
	</nav>
    <div id="empezar">
        <div class="row">
                    <div class="col-9">
                        <form id="rehilo" action="controlhilos.php" method="POST">
                            <fieldset>
                            <legend>Nuevo hilo</legend>
                              <div class="form-group">
                                <label for="titulo" style="margin-top: 10px;">Titulo</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Inserte un titulo" required="required">
                              </div>
                              <div class="form-group">
                                <label for="mensaje">Mensaje</label>
                                <textarea class="form-control" id="mensaje" name="mensaje" placeholder="Inserte un mensaje" required="required"></textarea>
                              </div>
                              <input type="submit" class="btn btn-primary" style="margin-left: 10px; margin-bottom: 5px;" name="Registrar" value="Registrar">
                              <button type="button" class="btn btn-danger" style="margin-left: 10px; margin-bottom: 5px;" name="cancelar" onclick="visualizarhilos()">Cancelar</button>
                            </fieldset>
                        </form>
                    </div>
        </div>
    </div>
    <div id="listahilos">
        <div class="container-fluid">
            <button type="button" style="margin-top: 10px;margin-left: 40px;" class="btn btn-success" onclick="visualizarrehilo()">Iniciar Hilo</button>
            <?php
                if ($_SESSION['rol']=='Cliente') {
                    $conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
                    if (mysqli_connect_errno()) {
                        printf("Conexión fallida %s\n", mysqli_connect_error());
                        exit();
                    }
                }
                if ($_SESSION['rol']=='Veterinario') {
                    $conexion = mysqli_connect('localhost', 'Veterinario', 'Veterinario', 'veterinario');
                    if (mysqli_connect_errno()) {
                        printf("Conexión fallida %s\n", mysqli_connect_error());
                        exit();
                    }
                }
                
                $sql="SELECT idhilo,tituloHilo,mensajeTitulo,fechaCreacion,usuLogin from hilos,usuarios where usuarios.dniUsu = hilos.usuCreador order by fechaCreacion desc;";
                $result = mysqli_query($conexion, $sql);
                if(mysqli_num_rows($result) > 0) {
                while ($registro = mysqli_fetch_row($result)) {
                    $date= date('d-m-Y', strtotime($registro[3]));
            ?>
                <div class="row" style="margin-top: 5px;">
                    <div class="col-md-6 p-3" style="margin-top: 10px;margin-left: 200px;margin-bottom: 10px;background-color: white;box-shadow: 0px 0px 8px grey; background-color: #ECF8E0;">
                        <h3 class="text-center"><?php echo $registro[1]; ?></h3>
                        <p><form action="" method="POST"><button type="submit" class="btn btn-primary float-right" value="<?php echo $registro[0].','.$registro[1].','.$registro[2]; ?>" name="id[]" style="margin-bottom: 10px;margin-left: 10px;">Participar</button></form><?php echo $registro[2]; ?></p>
                        <p><small class="float-right"><b style="letter-spacing: 1px;">Fecha:  <?php echo $date; ?></b></small><small ><b>Creador: <?php echo $registro[4]; ?></b></small>
                        </p>
                        

                    </div>
                </div>
            <?php
        }
    }else{
        echo "<h3>No hay hilos iniciados</h3>"; 
        }
        ?>
        </div>
    </div>
</body>
</html>