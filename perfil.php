<?php
session_start();

if (isset($_POST['cerrar'])) {
	session_destroy();

	header("Location:index.php");
}
$conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
			if (mysqli_connect_errno()) {
			    printf("Conexión fallida %s\n", mysqli_connect_error());
			    exit();
			}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home - Perfil</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	nav{
            background-image: linear-gradient(to left,lightblue, lightgreen);
        }
        *{
            letter-spacing: 1px;
        }
        body{
             background-image: linear-gradient(to right,lightblue, lightgreen);
        }
    </style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light">
  		<a class="navbar-brand" href="#">Animal Home</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
 		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
    		<ul class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link" href="inicio.php">Inicio</a></li>
						<li class="nav-item"><a class="nav-link"href="foro.php">Foro</a></li>
						<li class="nav-item"><a class="nav-link" href="mascotas.php">Mascotas</a></li>
						<li class="nav-item"><a class="nav-link" href="observacionescli.php">Observaciones</a></li>
						<li class="nav-item active"><a class="nav-link" href="perfil.php">Perfil</a></li>
					</ul>
					<form class="form-inline my-2 my-lg-0" action="" method="POST">
                         <button class="btn btn-danger my-2 my-sm-0" type="submit" name="cerrar">Cerrar Sesion</button>
                    </form>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
		        <img src="Imagenes/perfil.png" class="d-none d-md-block text-md-left img-thumbnail" style="border-color: black;margin-bottom: 20px; margin-top: 20px;margin-left: 100px;" >
		    </div>
		    <div class="col-md-6">
		    	<div class="table-responsive" style="margin-top: 20px; margin-left: 100px;">
            		<table class="table table-bordered">
            			<?php
            				$dni = $_SESSION['nif'];
            				$sql= "SELECT * from usuarios where dniUsu='$dni';";
            				$result = mysqli_query ($conexion, $sql);
                    		$registro=mysqli_fetch_row($result);
            			?>
            			<tr>
		                    <th class="bg-dark text-light">Nombre usuario</th>
		                    <td class="table-light"><?php echo $registro[1]; ?></td>
		                </tr>
		                <tr>
		                    <th class="bg-dark text-light">Nombre y Apellidos</th>
		                    <td class="table-light"><?php echo $registro[5] ." " .$registro[6]; ?></td>
		                </tr>
		                <tr>
		                    <th class="bg-dark text-light">Telefono</th>
		                    <td class="table-light"><?php echo $registro[7]; ?></td>
		                </tr>
		                <tr>
		                    <th class="bg-dark text-light">DNI</th>
		                    <td class="table-light"><?php echo $registro[0]; ?></td>
		                </tr>
		                <tr>
		                    <th class="bg-dark text-light">Correo</th>
		                    <td class="table-light"><?php echo $registro[8]; ?></td>
		                </tr>
            		</table>
            	</div>
            	
		    </div>
		</div>
	</div>