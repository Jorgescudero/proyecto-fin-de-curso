<?php
session_start();

$conexion = mysqli_connect('localhost', 'Cliente', 'Cliente', 'veterinario');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal Home</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	body{
    		background-image: linear-gradient(to left,lightblue, lightgreen);
    		size: cover;
    	}
    	#emailbusqueda{
			margin-top: 10px;
    		margin-left: 33%;
    		margin-bottom: 10px;
    		background-color: white;
    		box-shadow: 0px 0px 8px grey;
    	}
    	.jumbotron{
    		background-image: linear-gradient(to right,lightblue, lightgreen);
    	}
    	*{
            letter-spacing: 1px;
        }
    </style>
</head>
<body>
	<header class="jumbotron text-center">
            <h1>Animal Home</h1>
            <h4>Tu red de contacto con dueños y veterinarios</h4>
    </header>
    <div class="container">
		<div id="email">
			<div class="row">
						<div class="col-9">
							<form action="" method="POST" id="emailbusqueda" class="p-3">
								<div class="form-group">
									<label style="margin-left:5px" for="emailbus">Introduce tu email</label>
									<input type="email" class="form-control border-right-0 border-top-0 border-left-0" id="emailbus" name="emailbus" placeholder="Email" required="required">
								</div>
								<input type="submit" class="btn btn-primary" style="margin-left: 10px; margin-bottom: 5px;" name="inibusqueda" value="Comprobar"></input>
							</form>
							<?php
							if (isset($_POST['inibusqueda'])) {
								$mail=$_POST['emailbus'];
								$sql="SELECT usuCorreo from usuarios where usuCorreo='$mail';";
								$result = mysqli_query ($conexion, $sql);
								if(mysqli_num_rows($result) > 0) {
									$d=rand(10000,99999);
									$_SESSION['codigo']=$d;
									$_SESSION['email']=$_POST['emailbus'];
									$asunto = "Nueva contraseña";
									$texto = "Codigo nueva contraseña: " .$d ."\r\n.";
									$headers = "MIME-Version: 1.0" ."\r\n";
									$headers .= "Content-type: text/html; charset=utf-8" ."\r\n";
									$headers .= "From:noreplyanimalhome@gmail.com";
									mail($mail,$asunto,$texto,$headers);
									header("Location:nuevacontraseña.php");
								}else{
									echo "<h6>Ese email no esta registrado en Animal Home</h6>";
								}
							}
							?>
						</div>
			</div>
		</div>	
	</div>
</body>
</html>
